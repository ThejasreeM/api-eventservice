/**
 * EventController can control all CRUD responses
 */
package com.wavelabs.in.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import com.wavelabs.in.model.Event;
import com.wavelabs.in.model.PushNote;
import com.wavelabs.in.model.RestMessage;
import com.wavelabs.in.service.impl.EventServiceImpl;

/**
 * @author thejasree
 *
 */
@RestController
@Component
@CrossOrigin
@RequestMapping(path = "/api")
public class EventController {
	private String url = "http://10.9.8.182:8014/send";
	private String TOPIC = "Users";
	private String TYPE = "EVENTS";
	private static final String FIREBASE_SERVER_KEY = "AAAApIuus8k:APA91bGOVSZOStKK0xlq3P5dcElH88QGHjE2eDZOutNrJgKUVDBYe0o-RgUvqJPJbYMLqv-tCebc7r4qqyshbbiFq9ETjFnHCNbLvuissz2btbcTslRhX8HLk09RdBgf5TkjPQS82Qlz";
	@Autowired
	EventServiceImpl eventServiceImpl;

	@Autowired
	RestMessage restMessage;

	/*
	 * add event response
	 */
	@CrossOrigin
	@RequestMapping(path = "/event", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public ResponseEntity<RestMessage> addEvent(@RequestBody Event event) {
		boolean status = eventServiceImpl.addEvent(event);
		if (status) {
			// call push note service
			boolean value = sendNotification(event);
			System.out.println(value);
			restMessage.messageCode = "200";
			restMessage.message = "Event has been created successfully.";
			return ResponseEntity.status(200).body(restMessage);
		} else {
			restMessage.messageCode = "404";
			restMessage.message = "Event creation failed.";
			return ResponseEntity.status(404).body(restMessage);
		}
	}

	private boolean sendNotification(Event event) {
		HttpHeaders responseHeaders = new HttpHeaders();
		responseHeaders.set("Authorization", "key=" + FIREBASE_SERVER_KEY);
		responseHeaders.set("Content-Type", "application/json");
		RestTemplate restTemplate = new RestTemplate();
		try {

			PushNote body = null;
			// Map<String, String> notification = null;
			Map<String, String> data = null;

			body = new PushNote();
			body.setTo("/topics/" + TOPIC);
			body.setPriority("high");
			System.out.println("BODY:" + body);

//			notification = new HashMap<>();
//			notification.put("title", event.getTitle());
//			notification.put("body", "Events has been added.");
//			System.out.println("NOTIFICATION:" + notification);
			// body.setNotification(notification);

			data = new HashMap<>();
			data.put("title", event.getTitle());
			data.put("agenda", event.getAgenda());
			data.put("date", event.getDate().toString());
			data.put("id", event.getId().toString());
			data.put("image", event.getImage());
			data.put("title", event.getTitle());
			data.put("event_time", event.getEvent_time());
			data.put("mobile", event.getMobile());
			data.put("venue", event.getVenue());
			data.put("website", event.getWebsite());
			data.put("type", TYPE);
			body.setData(data);

			HttpEntity<PushNote> entity = new HttpEntity<>(body);
			System.out.println(body);
			restTemplate.exchange(url, HttpMethod.POST, entity, String.class);
			return true;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return false;

	}

	/*
	 * update event response
	 */
	@CrossOrigin
	@RequestMapping(path = "/event/{id}", method = RequestMethod.PUT)
	public ResponseEntity<RestMessage> editEvent(@PathVariable("id") long id, @RequestBody Event event) {
		Event updateEvent = eventServiceImpl.updateEvent(id, event);
		if (updateEvent == null) {
			restMessage.messageCode = "404";
			restMessage.message = "News updation failed.";
			return ResponseEntity.status(404).body(restMessage);
		}
		updateEvent.setAgenda(event.getAgenda());
		updateEvent.setId(event.getId());
		updateEvent.setDate(event.getDate());
		updateEvent.setImage(event.getImage());
		updateEvent.setTitle(event.getTitle());
		updateEvent.setWebsite(event.getWebsite());
		updateEvent.setVenue(event.getVenue());
		updateEvent.setMobile(event.getMobile());
		updateEvent.setEvent_time(event.getEvent_time());
		eventServiceImpl.updateEvent(id, updateEvent);
		restMessage.messageCode = "200";
		restMessage.message = "News has been updated successfully.";
		return ResponseEntity.status(200).body(restMessage);

	}

	/*
	 * get all events response
	 */
	@CrossOrigin
	@RequestMapping(path = "/event/all", method = RequestMethod.GET)
	public ResponseEntity<?> getAllEvents() {
		List<Event> event = eventServiceImpl.getAllEvents();
		if (event.isEmpty()) {
			return ResponseEntity.status(200).body(event);
		}
		return ResponseEntity.status(200).body(event);

	}

	/*
	 * get event response
	 */
	@SuppressWarnings("rawtypes")
	@CrossOrigin
	@RequestMapping(path = "/event/{id}", method = RequestMethod.GET)
	public ResponseEntity getEvent(@PathVariable("id") long id) {
		Event event = eventServiceImpl.getEvent(id);
		if (event == null) {
			return ResponseEntity.status(200).body(event);
		}
		return ResponseEntity.status(200).body(event);
	}

	/*
	 * delete response
	 */
	@CrossOrigin
	@RequestMapping(path = "/event/{id}", method = RequestMethod.DELETE)
	public ResponseEntity<RestMessage> removeEvent(@PathVariable("id") long id) {
		boolean status = eventServiceImpl.deleteEvent(id);
		if (status) {
			restMessage.messageCode = "200";
			restMessage.message = "Event has been deleted successfully.";
			return ResponseEntity.status(200).body(restMessage);
		}
		restMessage.messageCode = "404";
		restMessage.message = "Event deletion failed.";
		return ResponseEntity.status(404).body(restMessage);

	}
}

package com.wavelabs.in;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

/*
 * Application starts from EventsApiApplication.
 */
@ComponentScan("com.wavelabs.in")
@EntityScan("com.wavelabs.in.model")
@SpringBootApplication(scanBasePackages = "com.wavelabs.in")
@EnableJpaRepositories("com.wavelabs.in.repo")
public class EventsApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(EventsApiApplication.class, args);
	}
}

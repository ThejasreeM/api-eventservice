/**
 * EventService handles all service methods for CRUD operations.
 */
package com.wavelabs.in.service;

import java.util.List;

import com.wavelabs.in.model.Event;

/**
 * @author thejasree
 *
 */
public interface EventService {
	
	public boolean addEvent(Event event);
	public Event editEvent(Event event, long id);
	public List<Event> getAllEvents();
	public Event getEvent(long id);
	public boolean removeEvent(long id);

}

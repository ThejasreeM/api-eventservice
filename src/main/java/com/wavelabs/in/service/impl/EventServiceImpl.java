/**
 * NewsServiceImpl is the implementation class for EventService
 *         interface. Here we are implementing all CRUD methods.
 */
package com.wavelabs.in.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.wavelabs.in.model.Event;
import com.wavelabs.in.repo.EventRepository;

/**
 * @author thejasree
 *
 */
@Component
@Transactional
public class EventServiceImpl {
	
	@Autowired
	private EventRepository eventRepo;

	/*
	 * add event
	 */
	public boolean addEvent(Event event) {
		try {
			eventRepo.save(event);
			return true;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return false;
	}

	/*
	 * update event
	 */
	public Event updateEvent(long id, Event event) {
		try {
			event.setId(id);
			return eventRepo.save(event);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	/*
	 * get all events
	 */
	public List<Event> getAllEvents() {
		try {
			return (List<Event>) eventRepo.findAll();

		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	/*
	 * 
	 * get event
	 */
	public Event getEvent(long id) {
		try {
			return eventRepo.findById(id);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	/*
	 * delete event
	 */

	public Boolean deleteEvent(long id) {
		try {
			eventRepo.deleteById(id);
			return true;

		} catch (Exception e) {
			e.printStackTrace();
		}
		return false;
	}
	
}

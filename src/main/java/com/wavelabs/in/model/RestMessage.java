package com.wavelabs.in.model;

import org.springframework.stereotype.Component;

/**
 * 
 * @author thejasree RestMessage class is a POJO class which handles status
 *         codes and messages for services.
 */
@Component
public class RestMessage {

	public String message;
	public String messageCode;

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getMessageCode() {
		return messageCode;
	}

	public void setMessageCode(String messageCode) {
		this.messageCode = messageCode;
	}

}

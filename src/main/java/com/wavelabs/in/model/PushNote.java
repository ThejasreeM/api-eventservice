package com.wavelabs.in.model;

import java.util.Map;

/**
 * 
 * @author thejasree
 *
 */
public class PushNote {

	private String to;
	private Map<String, String> data;
	//private Map<String, String> notification;
	private String priority;
	public String getTo() {
		return to;
	}
	public void setTo(String to) {
		this.to = to;
	}
	public Map<String, String> getData() {
		return data;
	}
	public void setData(Map<String, String> data) {
		this.data = data;
	}
//	public Map<String, String> getNotification() {
//		return notification;
//	}
//	public void setNotification(Map<String, String> notification) {
//		this.notification = notification;
//	}
	public String getPriority() {
		return priority;
	}
	public void setPriority(String priority) {
		this.priority = priority;
	}
	

	

	

}

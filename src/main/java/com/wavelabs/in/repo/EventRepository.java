/**
 * Event Repo is an interface to manage JPA methods like save(),delete(), deleteAll(), findAll(), ..
 */
package com.wavelabs.in.repo;

import org.springframework.data.repository.CrudRepository;

import com.wavelabs.in.model.Event;

/**
 * @author thejasree
 *
 */
public interface EventRepository extends CrudRepository<Event, String> {

	Event findById(Long id);

	int deleteById(Long id);

}

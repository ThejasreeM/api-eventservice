FROM java:8
ADD target/eventservice.jar eventservice.jar
ENTRYPOINT ["java","-jar","eventservice.jar"]
